# Design notes

## Handling of external signon services

This handles authentication against external providers, and mapping
authenticated identities to site users.

## `models.Identity`

Represents an identity in an external authentication providers.

`Identity.person` is a nullable foreign key, pointing to the site user that
logs in using this identity and provider. If it is set, the Identity is
considered *bound*, else it's *unbound*.

A user who signs in using an unbound Identity is still considered anonymous by
Django's authentication, and the site can define procedures for binding
identities to actual users.

## providers

Represent authentication providers and how to interact with them.


# Using signon

## Adapt the User model

Provide `get_user_model().objects.get_housekeeper()` to return a user object to
use to sign `Identity` audit logs.

If `SIGNON_AUTO_CREATE_USER` is set to True, provide
`get_user_model().objects.create_from_identity(identity: signon.models.Identity)`
to create user objects from a `signon.models.Identity` object.


## Define a login view

Create a `TemplateView` which inherits from `signon.views.LoginMixin` and
points to a login template. For example:

```py
from signon.views import LoginMixin

class Login(LoginMixin, NM2LayoutMixin, TemplateView):
    template_name = "login.html"
```

The login template can include `signon/login.html` for the core part, for example:

```html+mako
{% extends "nm2-base.html" %}
{% load i18n %}

{% block content %}

<h1>{% trans "nm.debian.org login" %}</h1>

{% include "signon/login.html" %}

{% endblock %}
```

Route the login view as an url of your choice.

## Add signon to site urls

For example:

```py
urlpatterns = [
    path('signon/', include("signon.urls")),
]
```


## Testing

Create a base fixture that:

* Defines `self.user1` and `self.user2` as two distinct users
* Has a `NamedObject` `self.identities` that stores identity objects for the
  tests

For example:

```py
class SignonFixtureMixin(BaseFixtureMixin):
    """
    Base test fixture for signon tests
    """
    @classmethod
    def setUpClass(cls):
        from signon.models import Identity
        super().setUpClass()
        cls.add_named_objects(
            identities=NamedObjects(Identity),
        )
        cls.create_person("user1", status=const.STATUS_DD_NU)
        cls.create_person("user2", status=const.STATUS_DD_NU)

        cls.user1 = cls.persons.user1
        cls.user2 = cls.persons.user2
```

Set `SIGNON_TEST_FIXTURE` in settings to point to that mixin, for example:

```py
SIGNON_TEST_FIXTURE = "backend.unittest.SignonFixtureMixin"
```
