from __future__ import annotations
from django.utils.translation import ugettext as _
from rest_framework import authentication
from rest_framework.exceptions import AuthenticationFailed


class ApiKeyAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        """
        Get the user from the Api-Key: header set in the request

        Returns the User if one was found, else None
        """
        from . import models as amodels
        value = request.META.get("HTTP_API_KEY", None)
        if value is None:
            return None

        try:
            key = amodels.Key.objects.get(value=value)
        except amodels.Key.DoesNotExists:
            raise AuthenticationFailed(_("Invalid token provided in Api-Key header"))

        amodels.AuditLog.objects.create(
            key=key,
            key_enabled=key.enabled,
            remote_addr=request.META.get("REMOTE_ADDR", "(unknown)"),
            request_method=request.method,
            absolute_uri=request.build_absolute_uri(),
        )

        if not key.enabled:
            raise AuthenticationFailed(_("Invalid token provided in Api-Key header"))

        return (key.user, value)

    def authenticate_header(self, request):
        return None
