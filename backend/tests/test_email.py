from __future__ import annotations
from typing import List
from django.test import TestCase
from django.core import mail
from backend.email import StoredEmail
import tempfile
import contextlib
import mailbox
import email.message
import email


class TestStoredEmail(TestCase):
    @contextlib.contextmanager
    def mbox(self, messages: List[str]):
        with tempfile.NamedTemporaryFile(suffix=".mbox") as tf:
            mb = mailbox.mbox(tf.name)
            try:
                for msg in messages:
                    mb.add(email.message_from_string(msg.strip()))
            finally:
                mb.close()
            yield tf.name

    def test_plain(self):
        messages = [
            """
From: Enrico Zini <enrico@debian.org>
Date: Sat, 29 Feb 2020 16:27:47 +0100
Subject: test message

Test body
""", """
From: Enrico Zini <enrico@debian.org>
Date: Sat, 28 Feb 2020 16:27:47 +0100
Subject: earlier test message

Earlier test body
""",
        ]

        with self.mbox(messages) as fname:
            res = StoredEmail.get_mbox_jsonable(fname)

        self.assertEqual(res, [
            {
                "from": "Enrico Zini <enrico@debian.org>",
                "date_rfc": "Sat, 28 Feb 2020 16:27:47 +0100",
                "date_iso": "2020-02-28T16:27:47+01:00",
                "subject": "earlier test message",
                "body": "Earlier test body",
            }, {
                "from": "Enrico Zini <enrico@debian.org>",
                "date_rfc": "Sat, 29 Feb 2020 16:27:47 +0100",
                "date_iso": "2020-02-29T16:27:47+01:00",
                "subject": "test message",
                "body": "Test body",
            }
        ])

    def test_missing_date(self):
        messages = [
            """
From: Enrico Zini <enrico@debian.org>
Date: Sat, 29 Feb 2020 16:27:47 +0100
Subject: test message

Test body
""", """
From: Enrico Zini <enrico@debian.org>
Subject: earlier test message

Earlier test body
""",
        ]

        with self.mbox(messages) as fname:
            res = StoredEmail.get_mbox_jsonable(fname)

        self.assertEqual(res, [
            {
                "from": "Enrico Zini <enrico@debian.org>",
                "date_rfc": None,
                "date_iso": None,
                "subject": "earlier test message",
                "body": "Earlier test body",
            }, {
                "from": "Enrico Zini <enrico@debian.org>",
                "date_rfc": "Sat, 29 Feb 2020 16:27:47 +0100",
                "date_iso": "2020-02-29T16:27:47+01:00",
                "subject": "test message",
                "body": "Test body",
            }
        ])

    def test_missing_timezone(self):
        messages = [
            """
From: Enrico Zini <enrico@debian.org>
Date: Sat, 29 Feb 2020 16:27:47 +0100
Subject: test message

Test body
""", """
From: Enrico Zini <enrico@debian.org>
Date: Sat, 28 Feb 2020 16:27:47
Subject: earlier test message

Earlier test body
""",
        ]

        with self.mbox(messages) as fname:
            res = StoredEmail.get_mbox_jsonable(fname)

        self.assertEqual(res, [
            {
                "from": "Enrico Zini <enrico@debian.org>",
                "date_rfc": "Sat, 28 Feb 2020 16:27:47",
                "date_iso": "2020-02-28T16:27:47+00:00",
                "subject": "earlier test message",
                "body": "Earlier test body",
            }, {
                "from": "Enrico Zini <enrico@debian.org>",
                "date_rfc": "Sat, 29 Feb 2020 16:27:47 +0100",
                "date_iso": "2020-02-29T16:27:47+01:00",
                "subject": "test message",
                "body": "Test body",
            }
        ])


class TestNonce(TestCase):
    def test_send_challenge(self):
        from backend.email import send_nonce
        from backend.models import Person

        self.maxDiff = None
        person = Person.objects.create_user("test@example.org", fullname="Test Person", audit_skip=True)
        mail.outbox = []

        send_nonce("notification_mails/newperson.txt", person, encrypted_nonce="♥")
        self.assertEqual(len(mail.outbox), 1)
        m = mail.outbox[0]
        self.assertEqual(m.from_email, "nm@debian.org")
        self.assertEqual(m.to, ["Test Person <test@example.org>"])
        self.assertEqual(m.cc, [])
        self.assertEqual(m.bcc, [])
        self.assertEqual(m.extra_headers, {'Reply-To': 'nm@debian.org'})
        self.assertEqual(m.body, """Hello Test Person,

to confirm your new entry at https://nm.debian.org/person/test@example.org/
you need to decrypt the following text. The result will be a URL, which you
then visit to make your entry confirmed.

♥

You should not need instructions to decrypt this. If you do not know how to do
it, you can consider it a challenge. In that case, you can start from here:
https://www.dewinter.com/gnupg_howto/english/GPGMiniHowto.html

For any problem, feel free to contact Front Desk at nm@debian.org.


Regards,

the nm.debian.org robotic minion for Front Desk""")
        # self.assertIn(reverse("process_emeritus_self") + "?t=", mail.outbox[0].body)
        # self.assertIn(reverse("process_cancel", args=[process.pk]), mail.outbox[0].body)
        # self.assertIn(process.get_absolute_url(), mail.outbox[0].body)
        # self.assertIn("test ping", mail.outbox[0].body)
